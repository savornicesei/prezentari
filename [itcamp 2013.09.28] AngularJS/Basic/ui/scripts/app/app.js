﻿'use strict';

//var app = angular.module('demo', ['Exemplul1', 'Exemplul2']);

var app = angular.module('demo', ['Exemplul1', 'Exemplul2', 'Exemplul3'])
	.run(function ($rootScope) {
		//underscore.js
		$rootScope._ = window._;
		$rootScope.calculateTotals = function (fields, list, filter) {
			var totals = [];
			var flist = list; //filtered list to apply total to

			if (!$rootScope._.isNull(filter) && !$rootScope._.isUndefined(filter)) {
				flist = $rootScope._.filter(list, function (obj) {
					return obj[filter[0]] == filter[1];
				});
			};

			$rootScope._.each(fields, function (field) {
				totals.push($rootScope._(_(flist).pluck(field)).reduce(function (item1, item2) { return item1 + item2 }));
			});

			return $rootScope._.object(fields, totals);
		};
	})
	.filter('formatNegative', function () {
		//see formats defined for .NET http://msdn.microsoft.com/en-us/library/system.globalization.numberformatinfo.currencynegativepattern.aspx
		return function (input, format) {
			if (!jQuery.isNumeric(input) || input.toString().indexOf('-') != 0) {
				return input;
			}

			var output = input;
			switch (format) {
				case '(n)':
					output = "(" + input.replace('-', '') + ")";
					break;
				case '-n':
					output = input.toString();
					break;
				default:
					output = input
					break;
			}

			return output;
		}
	});