﻿'use strict';

var ex1 = angular.module('Exemplul1', []);

ex1.controller('Exemplul1Ctrl', ['$scope', function ($scope) {
	var model = {
		mesaj: 'Bine ati venit la prezentarea despre AngularJS',
		datanasterii: {
			an: null,
			luna: null,
			zi: null
		}
	};
	$scope.model = model;
	$scope.luni = ['ianuarie', 'februarie', 'martie', 'aprilie', 'mai', 'iunie',
			   'iulie', 'august', 'septembrie', 'octombrie', 'noiembrie', 'decembrie'];

	$scope.afiseazaVarsta = function () {
		var varsta = (new Date()).getFullYear() - parseInt($scope.model.datanasterii.an); console.log(varsta);
		$scope.model.mesaj = model.mesaj + ', chiar si la ' + varsta.toString() + (varsta > 1 ? ' ani!' : ' an!');
	};
}
]);