﻿'use strict';

var ex2 = angular.module('Exemplul2', []);

ex2.service('Exemplul2Service', ['$http', function ($http) {
	return {
		Adrese: function (strada) {
			console.log(strada);
			return $http({ url: ngdemo.baseUrl +'/data/adrese/' + strada, method: 'GET'}).then(function (result) { return result.data; });
		}
	}
} ]);

ex2.controller('Exemplul2Ctrl', function ($scope, Exemplul2Service) {
	Exemplul2Service.Adrese('Brates').then(function (d) {
		console.log(d);
		$scope.adrese = d;
		$scope.total = $scope.adrese.length;
		$scope.predicate = 'county';
	});
});

//var ex2 = angular.module('Exemplul2', ['adreseService']);
//ex2.controller('Exemplul2Ctrl', ['$scope', 'adreseService', function ($scope, Adrese) {
//	$scope.adrese = Adrese.get({ strada: 'Brates' });
//	$scope.total = $scope.adrese.length;
//} ]);