﻿angular.module("adreseService", ["ngResource"])
	   .factory("Adrese", function ($resource) {
			return $resource(
				   "/data/adrese/:strada",
				   { strada: "@strada" })
	   });