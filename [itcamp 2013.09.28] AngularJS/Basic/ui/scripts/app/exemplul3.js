﻿'use strict';

var ex3 = angular.module('Exemplul3', []);

ex3.service('Exemplul3Service', ['$http', function ($http) {
	return {
		Adrese: function (strada) {
			return $http({ url: ngdemo.baseUrl + '/data/adrese/' + strada, method: 'GET' }).then(function (result) { return result.data; });
		}
	}
} ]);

ex3.controller('Exemplul3Ctrl1', function ($scope, Exemplul3Service) {
	Exemplul3Service.Adrese('Brates').then(function (d) {
		$scope.adrese = d;
	});
	$scope.reverse = false;
	$scope.sortZip = function () {
		if ($scope.reverse) {
			$scope.adrese = $scope._.sortBy($scope.adrese, function (item) { return item.zip }).reverse();
		}
		else {
			$scope.adrese = $scope._.sortBy($scope.adrese, function (item) { return item.zip });
		}
		$scope.reverse = !$scope.reverse;
	};
});

ex3.controller('Exemplul3Ctrl2', function ($scope, Exemplul3Service) {
	Exemplul3Service.Adrese('Brates').then(function (d) {
		$scope.total = (-1) * d.length;
		console.log($scope.total);
	});
});