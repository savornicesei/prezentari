﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Simple.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Exemplul 2</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="container_12" ng-controller="Exemplul2Ctrl">
<span><h4>Raport sortabil folosind OrderBy</h4></span>
<br />
<table>
	<thead>
		<tr>
		<th><a href="" ng-click="predicate = 'county'">Judet</a></th>
			<th><a href="" ng-click="predicate = 'street_type'">Tipul strazii</a></th>
			<th><a href="" ng-click="predicate = 'zip'">Codul zip</a></th>
			<th><a href="" ng-click="predicate = 'description'">Descriere</a></th>
			<th><a href="" ng-click="predicate = 'sector'">Sector</a></th>
			<th><a href="" ng-click="predicate = 'location'">Localitate</a></th>
		</tr>
	</thead>
	<tbody>
		<tr ng-repeat="addr in adrese | orderBy: predicate">
			<td>{{addr.county}}</td>
			<td>{{addr.street_type}}</td>
			<td>{{addr.zip}}</td>
			<td>{{addr.description}}</td>
			<td>{{addr.sector}}</td>
			<td>{{addr.location}}</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5" style="font-weight: bold;">Total:</td>
			<td>{{total | number: 2}}</td>
		</tr>
	</tfoot>
</table>
</div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">	
</asp:Content>
