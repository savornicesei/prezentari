﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Simple.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Exemplul 1</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="container_12" ng-controller="Exemplul1Ctrl">
	<span><h3>{{model.mesaj}}!</h3></span><br />
	<span>Introduceti data Dvs. de nastere</span><br /><br />
	<span>An</span>&nbsp;
	<input type="text" id="an" ng-model="model.datanasterii.an" />&nbsp;&nbsp;
	<span ng-model="model.datanasterii.luna">Luna</span>&nbsp;
	<select id="luna" ng-model="model.datanasterii.luna" ng-options="l for l in luni"></select>&nbsp;&nbsp;
	<span>Zi</span>&nbsp;<input type="text" id="zi" ng-model="model.datanasterii.zi" />&nbsp;&nbsp;&nbsp;
	<button id="btnStart" ng-click="afiseazaVarsta()">Start</button>
</div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
