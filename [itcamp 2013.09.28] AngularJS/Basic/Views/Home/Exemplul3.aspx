﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Simple.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Exemplul 3</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div ng-controller="Exemplul3Ctrl1">
		<span>
			<h4>
				Raport sortabil folosind underscore.js</h4>
		</span>
		<br />
		<table>
			<thead>
				<tr>
					<th>
						Judet
					</th>
					<th>
						Tipul strazii
					</th>
					<th>
						<a href="" ng-click="sortZip()">Codul zip</a>
					</th>
					<th>
						Descriere
					</th>
					<th>
						Sector
					</th>
					<th>
						Localitate
					</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="addr in adrese">
					<td>
						{{addr.county}}
					</td>
					<td>
						{{addr.street_type}}
					</td>
					<td>
						{{addr.zip}}
					</td>
					<td>
						{{addr.description}}
					</td>
					<td>
						{{addr.sector}}
					</td>
					<td>
						{{addr.location}}
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div ng-controller="Exemplul3Ctrl2">
		<span>
			<h4>
				Controller 2</h4>
		</span>
		<br />
		<span>Total: {{total | formatNegative:(n)}}</span>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FooterContent" runat="server">
</asp:Content>
