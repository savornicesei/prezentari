﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Basic
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			//routes.MapRoute(
			//    "Default", // Route name
			//    "{controller}/{action}/{id}", // URL with parameters
			//    new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
			//);
			routes.MapRoute(
				"Acasa",
				"acasa",
				new { controller = "Home", action = "Default" }
			);
			routes.MapRoute(
				"Exemplul1",
				"exemplul1",
				new { controller = "Home", action = "Default" }
			);
			routes.MapRoute(
				"Exemplul2",
				"exemplul2",
				new { controller = "Home", action = "Exemplul2" }
			);
			routes.MapRoute(
				"Exemplul3",
				"exemplul3",
				new { controller = "Home", action = "Exemplul3" }
			);
			routes.MapRoute(
				"DataAdreseBrates",
				"data/adrese/{strada}",
				new { controller = "Data", action = "ObtineAdrese", strada = string.Empty }
			);
			routes.MapRoute(
				"Default",
				"{controller}/{action}", 
				new { controller = "Home", action = "Default"}
			);

		}

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			RegisterRoutes(RouteTable.Routes);
		}
	}
}