﻿using System.Web.Mvc;

namespace Basic.Controllers
{
	public class DataController : Controller
	{
		public ActionResult ObtineAdrese(string strada)
		{
			var jsonData = "[{\"county\":\"Braila\",\"street_type\":\"Strada\",\"zip\":\"810069\",\"description\":\"Brates nr. 1-19; 2-12\",\"sector\":\"\",\"location\":\"Braila\"},{\"county\":\"Prahova\",\"street_type\":\"Strada\",\"zip\":\"100582\",\"description\":\"Brates\",\"sector\":\"\",\"location\":\"Ploiesti\"},{\"county\":\"Mehedinti\",\"street_type\":\"Strada\",\"zip\":\"220113\",\"description\":\"Brates\",\"sector\":\"\",\"location\":\"Drobeta-Turnu S\"},{\"county\":\"Iasi\",\"street_type\":\"Strada\",\"zip\":\"700529\",\"description\":\"Brates\",\"sector\":\"\",\"location\":\"Iasi\"},{\"county\":\"Galati\",\"street_type\":\"Strada\",\"zip\":\"800175\",\"description\":\"Brates\",\"sector\":\"\",\"location\":\"Galati\"},{\"county\":\"Dolj\",\"street_type\":\"Strada\",\"zip\":\"200316\",\"description\":\"Brates\",\"sector\":\"\",\"location\":\"Craiova\"},{\"county\":\"Cluj\",\"street_type\":\"Alee\",\"zip\":\"400566\",\"description\":\"Brates nr. 5-T; 2-T\",\"sector\":\"\",\"location\":\"Cluj-Napoca\"},{\"county\":\"Cluj\",\"street_type\":\"Alee\",\"zip\":\"400522\",\"description\":\"Brates nr. 1-3\",\"sector\":\"\",\"location\":\"Cluj-Napoca\"},{\"county\":\"Braila\",\"street_type\":\"Strada\",\"zip\":\"810059\",\"description\":\"Brates nr. 21-T; 14-T\",\"sector\":\"\",\"location\":\"Braila\"},{\"county\":\"Bucuresti\",\"street_type\":\"Intrare\",\"zip\":\"031186\",\"description\":\"Brates\",\"sector\":\"3\",\"location\":\"Bucuresti\"}]";

			return Content(jsonData);
		}

	}
}
